package hu.thread.executor;

public class Start {

	public static void main(String[] args) {
	
		Thread t1 = new Thread(new Szamlalo("t1"));
		Thread t2 = new Thread(new Szamlalo("t2"));
		Thread t3 = new Thread(new Szamlalo("t3"));
		Thread t4 = new Thread(new Szamlalo("t4"));
		Thread t5 = new Thread(new Szamlalo("t5"));
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		
		//itt várjon az összes szál lefutására a program (fő szál)
		
		System.out.println("A fő szál fut tovább");
	}

}
