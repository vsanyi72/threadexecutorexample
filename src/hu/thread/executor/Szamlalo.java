package hu.thread.executor;

import java.util.Random;

public class Szamlalo implements Runnable {

	private String name;
	
	public Szamlalo(String name) {
		this.name = name;
	}
	
	@Override
	public void run() {
		System.out.println("[Start] Name: " + this.name);
		for(int i=0; i<5; i++) {
			System.out.println("[RUN] "+this.name +" - " + i);
			try {
				Thread.sleep(new Random().nextInt(3000)); //~0-3mp közötti véletlen 
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("[End] Name: " + this.name);
		
	}

}
